# OpenComposite - Play SteamVR games without SteamVR!

[![Discord](https://img.shields.io/discord/499733750209314816.svg)](https://discord.gg/zYA6Tzs)
[![AppVeyor](https://img.shields.io/appveyor/ci/ZNix/openovr.svg)](https://ci.appveyor.com/project/ZNix/openovr)

Development has now moved to official GitLab repo. Please go there for latest release.

Official GitLab OpenXR branch can be found here:

https://gitlab.com/znixian/OpenOVR/-/tree/openxr

Download links and instructions can be found on that page.

Support for OpenComposite can be found in the Discord:

https://discord.gg/zYA6Tzs


